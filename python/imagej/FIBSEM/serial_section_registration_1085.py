#@ File(label="Raw Source Directory", style="directory", value="/groups/cardona/cardonalab/FIBSEM_L1085", persist=false) SOURCE_DIR
#@ File(label="Target Directory", style="directory", value="/groups/cardona/cardonalab/Andrew/FIBSEM_L1085", persist=false) TARGET_DIR

#@ Boolean(label="Precompute matches", value=False, persist=false) ENSURE_MATCHES
#@ Boolean(label="Report missing matches", value=False, persist=false) REPORT_MISSING
#@ Boolean(label="Align", value=True, persist=false) ALIGN
#@ Boolean(label="View aligned", value=False, persist=false) VIEW_ALIGNED
#@ Boolean(label="Export N5", value=False, persist=false) EXPORT_N5
#@ Boolean(label="Rotate export 180deg", value=False, persist=false) ROTATE_EXPORT
#@ Boolean(label="Exit after finishing (headless workaround)", value=False, persist=false) EXIT

#@ Integer(label="Chunk total count", value=0, persist=false) CHUNK_COUNT
#@ Integer(label="Chunk start", value=0, persist=false) CHUNK_START
#@ Integer(label="Chunk limit", value=0, persist=false) CHUNK_LIMIT

#@ Integer(label="N5 slab", value=-1, persist=false) N5_SLAB

# Albert Cardona 2019-05-31
#
# A series of scripts to register FIBSEM serial sections.
# ASSUMES there is only one single image per section.
# ASSUMES all images have the same dimensions and pixel type.
# 
# This program is similar to the plugin Register Virtual Stack Slices
# but uses more efficient and densely distributed features,
# and also matches sections beyond the direct adjacent for best stability
# as demonstrated for elastic registration in Saalfeld et al. 2012 Nat Methods.
# 
# The program also offers functions to export for CATMAID.
#
# 1. Extract blockmatching features for every section.
# 2. Register each section to its adjacent, 2nd adjacent, 3rd adjacent ...
# 3. Jointly optimize the pose of every section.
# 4. Export volume for CATMAID.

import errno, inspect, os, sys, traceback
import json, math
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(inspect.getsourcefile(lambda:0)))) + "/IsoView-GCaMP/")
from mpicbg.models import TranslationModel2D
from mpicbg.imagefeatures import FloatArray2DSIFT
from net.imglib2 import FinalInterval
from net.imglib2.img.io.proxyaccess import ShortAccessProxy
from net.imglib2.type.numeric.integer import UnsignedByteType, UnsignedShortType
from lib.serial2Dregistration import align, ensurePointMatches, viewAligned, export8bitN5, loadImp, loadMatrices, matricesName, estimateBounds, reportMissingMatches


srcDir = SOURCE_DIR.toString()
tgtDir = TARGET_DIR.toString() # for CSV files

# Paths must end in trailing slash.
if srcDir[len(srcDir) - 1] != '/':
  srcDir += '/'
if tgtDir[len(tgtDir) - 1] != '/':
  tgtDir += '/'

exportDir = os.path.join(tgtDir, "n5/") # for the N5 volume

paramsFile = tgtDir + 'params.json'
paramsOptFile = tgtDir + 'params_opt.json'
filelistFile = tgtDir + 'filelist'

filelist = []
try:
  with open(filelistFile, 'r') as f:
    filelist = [line.rstrip('\n') for line in f]
except IOError as e:
  if e.errno == errno.ENOENT:
    filelist = os.listdir(srcDir)
    pass
  else:
    raise

filepaths = [os.path.join(srcDir, filename)
             for filename in sorted(filelist)
             if filename.endswith("InLens_raw.tif")]
interval = None #[[4096, 4096],
                # [12288 -1, 12288 -1]] # to open only that, or None
pixelType = UnsignedShortType
proxyType = ShortAccessProxy

# Parameters for blockmatching
params = {
 'scale': 0.1, # 10%
 'meshResolution': 1000, # in original pixels
 'minR': 0.1, # min PMCC (Pearson product-moment correlation coefficient)
 'rod': 0.9, # max second best r / best r
 'maxCurvature': 1000.0, # default is 10
 'searchRadius': 100, # a low value: we expect little translation
 'blockRadius': 200, # small, yet enough

  # Parameters for SIFT features, in case blockmatching fails due to large translation
  "siftFdSize": 8,
  "siftFdBins": 8,
  "siftMaxOctaveSize": 1024,
  "siftSteps": 3,
  "siftMinOctaveSize": 256,
  "siftInitialSigma": 1.6,
  "siftRod": 0.5,
}

try:
  with open(paramsFile, 'r') as f:
    params.update(json.load(f))
except IOError as e:
  if e.errno == errno.ENOENT:
    pass
  else:
    raise

# Parameters for computing the transformation models
paramsTileConfiguration = {
  "n_adjacent": 3, # minimum of 1; Number of adjacent sections to pair up
  "maxAllowedError": 0, # Saalfeld recommends 0
  "maxPlateauwidth": 200, # Like in TrakEM2
  "maxIterations": 2, # Saalfeld recommends 1000 -- here, 2 iterations (!!) shows the lowest mean and max error for dataset FIBSEM_L1116
  "damp": 1.0, # Saalfeld recommends 1.0, which means no damp
}

try:
  with open(paramsOptFile, 'r') as f:
    paramsTileConfiguration.update(json.load(f))
except IOError as e:
  if e.errno == errno.ENOENT:
    pass
  else:
    raise


print "Number of files: %i" % (len(filepaths),)
if CHUNK_COUNT != 0:
  size = int(math.ceil(len(filepaths) / float(CHUNK_COUNT)))
  start = size * CHUNK_START
  start = min(start, len(filepaths) - 1)
  end = start + size * CHUNK_LIMIT + paramsTileConfiguration["n_adjacent"] + 1 if CHUNK_LIMIT else len(filepaths)
  end = min(end, len(filepaths))
  print "Start: %i End: %i" % (start, end)
  filepaths = filepaths[start:end]

# Ensure target directories exist
if not os.path.exists(tgtDir):
  os.mkdir(tgtDir)

csvDir = os.path.join(tgtDir, "csvs")

if not os.path.exists(csvDir):
  os.mkdir(csvDir)

if ENSURE_MATCHES:
  ensurePointMatches(filepaths, csvDir, params, paramsTileConfiguration["n_adjacent"])

if REPORT_MISSING:
  reportMissingMatches(filepaths, csvDir, paramsTileConfiguration["n_adjacent"])

if ALIGN:
  # Run the alignment
  matrices = align(filepaths, csvDir, params, paramsTileConfiguration)
  print estimateBounds(filepaths, matrices)


dimensions = [5000, 5000]
if VIEW_ALIGNED:
  # Show only a cropped middle area
  x0 = 3 * dimensions[0] / 8
  y0 = 3 * dimensions[1] / 8
  x1 = x0 + 2 * dimensions[0] / 8 -1
  y1 = y0 + 2 * dimensions[1] / 8 -1
  print "Crop to: x=%i y=%i width=%i height=%i" % (x0, y0, x1 - x0 + 1, y1 - y0 + 1)
  interval = estimateBounds(filepaths, loadMatrices(matricesName(paramsTileConfiguration), csvDir))
  viewAligned(filepaths, csvDir, params, paramsTileConfiguration, dimensions, interval)

if EXPORT_N5:
  # Write the whole volume in N5 format
  name = srcDir.split('/')[-2]
  # Export ROI:
  # interval = FinalInterval([0, 0], [5000-1, 5000-1])
  interval = estimateBounds(filepaths, loadMatrices(matricesName(paramsTileConfiguration), csvDir))
  print interval

  slab = N5_SLAB if N5_SLAB >= 0 else None

  # Expects matrices csv file to exist already
  matricesCsv = matricesName(paramsTileConfiguration)
  export8bitN5(filepaths, dimensions, loadMatrices(matricesCsv, csvDir),
              name + '/s0', exportDir, interval, gzip_compression=6, block_size=[128, 128, 128],
              rotate=ROTATE_EXPORT,
              copy_threads=4, n5_threads=0, slab=slab,
              CLAHE_params=[400, 256, 2.0])

print "Done"
if EXIT:
  os._exit(0)
